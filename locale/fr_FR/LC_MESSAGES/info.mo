��          �      \      �     �     �     �          '  
   8     C     S     n     �     �     �     �     �     �               $     -  �  9     �     �     �  <   
  "   G     j  &   v     �  @   �  +   �  "   '  q   J     �      �  #   �          0  	   ?  8   I                  
                                               	                                  12-hour timekeeping 24-hour timekeeping animated weather icons include netatmo values info_description info_title invalid owm key not animated weather icons not include netatmo values owm api key example owm api key input owm api key link temperature in Kelvin temperature in celsius temperature in fahrenheit temperature unit time format validate which city? Project-Id-Version: mirr.OS v0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-17 10:20+0100
PO-Revision-Date: 2018-09-04 13:58+0200
Last-Translator: Marco Roth <info@marco-roth.ch>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 2.1.1
Language: fr_FR
 Format 12 heures Format 24 heures icônes météo animées Utilisation des valeurs Netatmo dans le module d'information Date, heure et météo aujourd'hui Information Invalide OpenWeatherWeatherMap Api-Key icônes météo non animées ne pas utiliser les valeurs Netatmo dans le module d'information (Exemple: c863d93d1c7ff4ef9bace44847e40e98) Entrer dans OpenWeatherMap Api-Key Ici, vous pouvez obtenir votre OpenWeather <a href="https://openweathermap.org/appid" target="_blank">Api-Key</a> Températures en Kelvin Températures en degrés Celsius Températures en degrés Fahrenheit unité de température format horaire validiera Pour quelle ville la météo doit-elle être affichée ? 