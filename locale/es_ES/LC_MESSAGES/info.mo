��          �      \      �     �     �     �          '  
   8     C     S     n     �     �     �     �     �     �               $     -  �  9     �     �     �  4        G     `  !   n     �  .   �  +   �  !   	  i   +     �  #   �  !   �     �       
     -   #                  
                                               	                                  12-hour timekeeping 24-hour timekeeping animated weather icons include netatmo values info_description info_title invalid owm key not animated weather icons not include netatmo values owm api key example owm api key input owm api key link temperature in Kelvin temperature in celsius temperature in fahrenheit temperature unit time format validate which city? Project-Id-Version: mirr.OS v0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-17 10:20+0100
PO-Revision-Date: 2018-09-04 14:05+0200
Last-Translator: Marco Roth <info@marco-roth.ch>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.1.1
Language: es_ES
 Formato de 12 horas Formato de 24 horas iconos animados del tiempo Uso de valores Netatmo en el módulo de información Fecha, hora y tiempo hoy Próximamente Api-Key OpenWeatherMap no válido iconos del tiempo no animados no utilizar valores Netatmo en el módulo info (Ejemplo: c863d93d1c7ff4ef9bace44847e40e98) Ingresar a OpenWeatherMap Api-Key Aquí puede obtener su OpenWeather <a href="https://openweathermap.org/appid" target="_blank">Api-Key</a> Temperaturas en Kelvin Temperaturas en grados centígrados Temperaturas en grados Fahrenheit unidad de temperatura formato de hora convalidar ¿Para qué ciudad se debe mostrar el tiempo? 