��          �      <      �     �     �     �     �               2     M     a     s     �     �     �     �     �     �     �  �  �  -   �  -     9   E  J     ,   �  9   �  M   1  (     &   �  u   �  9   E  ?     *   �     �     
     &  K   =                             
       	                                                                 12-hour timekeeping 24-hour timekeeping animated weather icons include netatmo values invalid owm key not animated weather icons not include netatmo values owm api key example owm api key input owm api key link temperature in celsius temperature in fahrenheit temperature in kelvin temperature unit time format validate which city? Project-Id-Version: mirr.OS Info module
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-27 09:41+0100
PO-Revision-Date: 2018-08-16 10:36+0800
Last-Translator: Tobias Grasse <tg@glancr.de>
Language-Team: glancr Team kontakt@glancr.de
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 12-часовой формат времени 24-часовой формат времени Значки с анимированной погодой Использовать значения Netatmo в инфо-модуле неверный ключ API OpenWeatherMap Неанимированные значки погоды. Не используйте значения Netatmo в инфо-модуле (e.g.: c863d93d1c7ff4ef9bace44847e40e98) Введите OpenWeatherMap Api-Key. Здесь вы получаете <a href="http://openweathermap.org/appid" target="_blank">openweather Api-Key</a>. температура в градусах Цельсия температура в градусах Фаренгейта температура в Кельвине блок температуры формат времени подтвердить Для какого города вы хотите знать погоду? 